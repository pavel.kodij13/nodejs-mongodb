const router = require('express').Router();
const {check} = require('express-validator')
const controller = require('../Controllers/authController')
const authMiddleWare = require("../middleware/authMiddleWare")
const roleMiddleWare = require("../middleware/roleMiddleWare")

router.post('/auth/login', controller.login)
router.get('/auth/users',roleMiddleWare(['USER']), controller.getUsers)
router.get('/users/me',authMiddleWare, controller.getUserProfile)

router.post('/auth/register', [
    check('username', 'This input can not be empty').notEmpty(),
    check('password', 'The password must contain 4-10 numbers').isLength({min:4, max:10}),

], controller.registration)

module.exports = router; 
