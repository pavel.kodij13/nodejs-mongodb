const express = require("express");
const PORT = 8080;
const mongoose = require('mongoose')
const authRouter = require('./routes/authRouter')
const notesRouter = require('./routes/notesRouter')

const app = express();
  
app.use(express.json())

app.use('/api',authRouter, notesRouter)

const start = async () => {
  try {
      await mongoose.connect(`mongodb+srv://cluster0.i4hyc.mongodb.net/myFirstDatabase`)
    app.listen(PORT, () => {
      console.log(`server started on port ${PORT}`);
    });
  } catch (error) {
    console.log(error);
  }
};
start();
