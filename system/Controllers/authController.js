const User = require("../models/User");
const Role = require("../models/Role");
const bcrypt = require("bcryptjs");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const { secret } = require("../config");

class authController {
  async registration(req, res) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({
          message: "Your password is not correct",
          errors,
        });
      }
      const { username, password } = req.body;
      const candidate = await User.findOne({ username });
      if (candidate) {
        return res.status(400).json({
          message: "Username already exists",
        });
      }
      const hashPassword = bcrypt.hashSync(password, 7);
      const userRole = await Role.findOne({ value: "USER" });

      const user = new User({
        username,
        password: hashPassword,
        roles: [userRole.value],
      });
      await user.save();

      return res.status(200).json({
        message: "Success",
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        message: "Error",
      });
    }
  }
  async login(req, res) {
    try {
      const { username, password } = req.body;
      const user = await User.findOne({ username });

      if (!user) {
        return res.status(400).json({
          message: `User with such username - ${username} is not exist`,
        });
      }

      const validPassword = bcrypt.compareSync(password, user.password);
      if (!validPassword) {
        return res.status(400).json({
          message: `Check your password`,
        });
      }

      const token = generateAccessToken(user._id, user.roles);
      return res.status(200).json({
        message: "Success",
        jwt_token: token,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        message: "Error",
      });
    }
  }
  async getUsers(req, res) {
    try {
      const users = await User.find();
      res.json(users);
    } catch (error) {
      console.log(error);
    }
  }

  async getUserProfile(req, res) {
    try {
      const users = await User.findOne(req.header._id);
      const user = {
        _id: users._id,
        username: users.username,
        createdDate: users.createdAt,
      };
      if (!users) {
        res.status(400).json({
          message: "User not found",
        });
      }

      if (users) {
        res.status(200).json({
          user,
        });
      }
    } catch (error) {
      res.status(500).json({
        message: "Error",
      });
    }
  }
}

const generateAccessToken = (id, roles) => {
  const payload = {
    id,
    roles,
  };

  return jwt.sign(payload, secret, {
    expiresIn: "24h",
  });
};

module.exports = new authController();
